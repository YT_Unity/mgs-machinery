﻿/*************************************************************************
 *  Copyright (C), 2015-2016, Mogoson tech. Co., Ltd.
 *  FileName: SyTelescopicArm.cs
 *  Author: Mogoson   Version: 1.0   Date: 12/24/2015
 *  Version Description:
 *    Internal develop version,mainly to achieve its function.
 *  File Description:
 *    Ignore.
 *  Class List:
 *    <ID>           <name>             <description>
 *     1.        SyTelescopicArm           Ignore.
 *  Function List:
 *    <class ID>     <name>             <description>
 *     1.
 *  History:
 *    <ID>    <author>      <time>      <version>      <description>
 *     1.     Mogoson     12/24/2015       1.0        Build this file.
 *************************************************************************/

namespace Developer.Machinery
{
    using UnityEngine;

    [AddComponentMenu("Developer/Machinery/SyTelescopicArm")]
	public class SyTelescopicArm : TelescopicArmMechanism
    {
        #region Public Method
		/// <summary>
		/// Drive the mechanism.
		/// </summary>
		/// <param name="speedControl">Speed control.</param>
		public override void DriveMechanism (float speedControl)
		{
			foreach (var joint in tJoints) 
			{
                joint.DriveMechanism(speedControl);
			}
		}//DriveM...()_end
        #endregion
    }//class_end
}//namespace_end